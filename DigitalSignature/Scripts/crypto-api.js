//**WEB_DEV team** (Центр биржевых информационных технологий)

//- [Sergei Startsev](https://bitbucket.org/sergei_startsev)

// ### JS Crypto API.

function CryptoAPI(options) {
	//private fields
	var piece = "base64,";

	var ENCODING_ERR = 5, 
	INVALID_MODIFICATION_ERR = 9,
	INVALID_STATE_ERR = 7, 
	NO_MODIFICATION_ALLOWED_ERR = 6,
	NOT_FOUND_ERR = 1,
	NOT_READABLE_ERR = 4,
	PATH_EXISTS_ERR = 12,
	QUOTA_EXCEEDED_ERR = 10,
	SECURITY_ERR = 2,
	TYPE_MISMATCH_ERR = 11;

    //public fileds
    var defaultOptions = {
    	//server url
        url: "http://localhost:7755",
        //methods
        getCertsURL: "getcerts",
        signStringURL: "signstring",
        signStringBase64URL: "signfile",
        signFileURL: "signfile",
        //Error messages
        connectionErrorMessage: "Не могу подключиться к Клиенту создания ЭЦП.\nУбедитесь, что клиент установлен, запущен и его версия соответствует актуальной http://butb.by/?page=51.",

        showMessage: function(message){
        	//alert(message);
        }
    };

    for(var option in defaultOptions){
    	this[option] = options && options[option]!==undefined ? options[option] : defaultOptions[option];
    }
    
    this.getCerts = function (successCallback, errorCallback) {
        /// <summary>
        /// Get certificate list
        /// </summary>
        /// <param name="successCallback">Success callback, get JSON {"CertificateList":[{"CertificateID":"...", "CertificateName":"..."}, {...}]}</param>
        /// <param name="errorCallback">Error callback, get error message</param>

        var xhr = createCORSRequest.call(this, 'POST', this.url + "/" + this.getCertsURL);

        processRequest.call(this, xhr, successCallback, errorCallback);

        xhr.send();
    };

    this.signString = function (idCert, password, string, timestamp, successCallback, errorCallback) {
        /// <summary>
        /// Sign string
        /// </summary>
        /// <param name="idCert">Certificate identificator</param>
        /// <param name="password">Container password</param>
        /// <param name="string">String for sign</param>
        /// <param name="timestamp">Server timestamp</param>
        /// <param name="successCallback">Success callback, get JSON {"SignatureInfo":{"Certificate":{...}, "SignatureBase64":{...}}}</param>
        /// <param name="errorCallback">Error callback, get error message</param>

        var params = "idCert=" + idCert + "~a7d@password=" + password + "~a7d@string=" + string + "~a7d@timestamp=" + timestamp;

        var xhr = createCORSRequest.call(this, 'POST', this.url + "/" + this.signStringURL);

        processRequest.call(this, xhr, successCallback, errorCallback);

        xhr.send(params);
    };

    this.signStringBase64 = function (idCert, password, stringBase64, timestamp, successCallback, errorCallback) {
        /// <summary>
        /// Sign string in BASE64 format
        /// </summary>
        /// <param name="idCert">Certificate identificator</param>
        /// <param name="password">Container password</param>
        /// <param name="stringBase64">String in BASE64 format for sign</param>
        /// <param name="timestamp">Server timestamp</param>
        /// <param name="successCallback">Success callback, get JSON {"SignatureInfo":{"Certificate":{...}, "SignatureBase64":{...}}} and file content in base64</param>
        /// <param name="errorCallback">Error callback, get error message</param>

        var params = "idCert=" + idCert + "~a7d@password=" + password + "~a7d@string=" + stringBase64 + "~a7d@timestamp=" + timestamp;

        var xhr = createCORSRequest.call(this, 'POST', this.url + "/" + this.signStringBase64URL);

        processRequest.call(this, xhr, successCallback, errorCallback);

        xhr.send(params);
    };

    this.signFile = function (idCert, password, file, timestamp, successCallback, errorCallback) {
        /// <summary>
        /// Sign file
        /// </summary>
        /// <param name="idCert">Certificate identificator</param>
        /// <param name="password">Container password</param>
        /// <param name="file">File for sign</param>
        /// <param name="timestamp">Server timestamp</param>
        /// <param name="successCallback">Success callback, get JSON {"SignatureInfo":{"Certificate":{...}, "SignatureBase64":{...}}} and file content in base64</param>
        /// <param name="errorCallback">Error callback, get error message</param>

        var params = "idCert=" + idCert + "~a7d@password=" + password + "~a7d@timestamp=" + timestamp;

        var _this = this;
        readFile(
            file,
            function (fileContentBase64) {
                var xhr = createCORSRequest.call(_this, 'POST', _this.url + "/" + _this.signFileURL);

                processRequest.call(_this, xhr, function (response) { successCallback(response, fileContentBase64) }, errorCallback);

                xhr.send(params + "~a7d@string=" + fileContentBase64);
            },
            errorCallback
        );
    };

    function createCORSRequest(method, url) {
        /// <summary>
        /// Create the XHR object for CORS requests.
        /// </summary>
        /// <param name="method">Method type (POST or GET)</param>
        /// <param name="url">Method name</param>
        /// <returns type="XMLHttpRequest">XHR object</returns>
	    var xhr = new XMLHttpRequest();

	    if ("withCredentials" in xhr) {
	    	// XHR for Chrome/Firefox/Opera/Safari/IE10.
	    	xhr.open(method, url, true);
	  	} else if (typeof XDomainRequest != "undefined") {
	    	// XDomainRequest for IE.
	    	xhr = new XDomainRequest();
	    	xhr.open(method, url);
	  	} else {
	    	// CORS not supported.
	    	this.showMessage("CORS not supported.");
	    	xhr = null;
	  	}
		return xhr;
	}

    function processRequest(xhr, successCallback, errorCallback) {
        /// <summary>
        /// Process request
        /// </summary>
        /// <param name="xhr">XHR object</param>
        /// <param name="successCallback">Success callback</param>
        /// <param name="errorCallback">Error callback</param>

		var _this = this;

		if (xhr){

			xhr.onload=function(result){
				//All is OK
				if(xhr.status==200){
				    successCallback(xhr.responseText);
				//Smth went wrong
				} else {
					_this.showMessage("Ошибка "+xhr.status+".\n"+xhr.responseText);
					errorCallback(xhr);
				}
			};

			xhr.onerror = function (result) {
			    _this.showMessage("Ошибка\n" + xhr.responseText);
			    if (xhr.responseText == "") {
			        xhr={responseText:_this.connectionErrorMessage, status: 500, statusText: ""};
			    }
			    errorCallback(xhr);
			};

			// setRequestHeader for Chrome/Firefox/Opera/Safari/IE10.
			if(typeof xhr.setRequestHeader != "undefined"){
				xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded; charset=UTF-8");
			// contentType for IE.
			}else{
				xhr.contentType = "application/x-www-form-urlencoded; charset=UTF-8";
			}

            if (typeof xhr.overrideMimeType != "undefined") {
                xhr.overrideMimeType('application/json; charset=UTF-8');
            }

		}
	}

    function readFile(file, successCallback, errorCallback) {
        /// <summary>
        /// Read file in base64 format.
        /// </summary>
        /// <param name="file">File for reading</param>
        /// <param name="successCallback">Success callback</param>
        /// <param name="errorCallback">Error callback</param>

		var _this = this;

		//create FileReader
		var reader = new FileReader();

	    // Closure to capture the file information.
	    reader.onload = function(event) {
	    	//cut 'data:URL...' piece from a base64 string
	    	successCallback(event.target.result.substring(event.target.result.indexOf(piece)+piece.length));
	    };

	    reader.onerror = function(event) {
	      	var message = "";
	      	switch (event.target.error.code) {
			   case ENCODING_ERR:
			    	message = "URL имеет неверный формат. Убедитесь, что URL является полным и достоверным.";
			    	break;

			   	case INVALID_MODIFICATION_ERR:
			    	message = "Изменение запроса не допускается.";
			    	break;

			    case INVALID_STATE_ERR:
			    	message = "Эта операция не может быть выполнена в текущем состоянии интерфейса объекта.";
			    	break;

			    case NO_MODIFICATION_ALLOWED_ERR:
			    	message = "Состояние файловой системы предотвращает любые записи в файл или каталог.";
			    	break;

			    case NOT_FOUND_ERR:
			    	message = "Файл или каталог не может быть прочитан.";
			    	break;

			    case PATH_EXISTS_ERR:
			    	message = "Файл или каталог с таким именем уже существует.";
			    	break;

			    case QUOTA_EXCEEDED_ERR:
			    	message = "Не хватает свободного места хранения или была достигнута квота хранилища.";
			    	break;

			    case SECURITY_ERR:
			    	message = "Файл может быть небезопасным для доступа в веб-приложении.";
			    	break;

			    case TYPE_MISMATCH_ERR:
			    	message = "Найденная запись имеет неправильный тип.";
			    	break;

			   	default:
					message = "Неизвестная ошибка.";
			    	break;
			}

			_this.showMessage("Ошибка\n"+message);
			errorCallback(message);			
		};

		reader.readAsDataURL(file);
	}
}